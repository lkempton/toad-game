﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuScript : MonoBehaviour
{
    public GameObject levelSelectPanel;
    private int lvlCompleted = 1;

    public Button[] buttons = new Button[20];

    public Color green = new Color(167f, 255f, 0f, 255f);
    public Color red = new Color(255f, 114f, 144f, 255f);

    void Start()
    {
        
    }

    public void StartGame()
    {
        levelSelectPanel.SetActive(true);
        SetButtonColours();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Back()
    {
        levelSelectPanel.SetActive(false);
    }

    void SetButtonColours()
    {
        lvlCompleted = PlayerPrefs.GetInt("levelsCompleted");

        Debug.Log(lvlCompleted);

        for (int i = 0; i <= 17; i++)
        {
            if (i < lvlCompleted)
            {
                ColorBlock cb = buttons[i].colors;
                cb.normalColor = green;
                buttons[i].colors = cb;
            }
            else
            {
                ColorBlock cb = buttons[i].colors;
                cb.normalColor = red;
                buttons[i].colors = cb;
            }

            SetScoreText(i + 1);
        }
    }

    public void ResetStats()
    {
        PlayerPrefs.SetInt("levelsCompleted", 1);
    }

    public void StartLevel(int levelNum)
    {
        lvlCompleted = PlayerPrefs.GetInt("levelsCompleted");

        if (levelNum <= lvlCompleted && levelNum < 10)
        {
            Application.LoadLevel("scene0" + levelNum);
        }
        else if (levelNum <= lvlCompleted && levelNum >= 10)
        {
            Application.LoadLevel("scene" + levelNum);
        }
    }

    void SetScoreText(int buttonNum)
    {
        int score;
        string requestScore = ("levelScore_" + buttonNum.ToString());
        score = PlayerPrefs.GetInt(requestScore);

        buttons[buttonNum - 1].GetComponentInChildren<Text>().text = "Level " + buttonNum + "    " + score + "/3";
    }
}
