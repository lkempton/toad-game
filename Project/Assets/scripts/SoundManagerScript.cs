﻿using UnityEngine;
using System.Collections;

public class SoundManagerScript : MonoBehaviour {

    // Use an array to store audio sources.
    private AudioSource[] audio = new AudioSource[1];

    void Start()
    {
        // Add each source to the array.
        audio = GetComponents<AudioSource>();
    }
    // Method that is called from other scripts, plays the sound source of the element in the array.
    public void PlayClips(int clip)
    {
        // Play the clip.
        //audio[clip].Play();
    }
}
