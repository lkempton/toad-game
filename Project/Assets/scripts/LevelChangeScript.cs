﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelChangeScript : MonoBehaviour {

    public int levelNum;

    public GameObject completedLevelCanvas;

    public GameControllerScript gc;

    public Text pickupText;

    [SerializeField]
    private bool isFinalLevel = false;

    void Start()
    {
        gc = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && isFinalLevel == false)
        {
            completedLevelCanvas.SetActive(true);

            pickupText.text = "Pickups Collected: " + gc.pickupNum.ToString() +"/3";

            UpdatePlayerPrefs();
            Time.timeScale = 0f;
        }
        else if (other.gameObject.CompareTag("Player") && isFinalLevel == true)
        {
            completedLevelCanvas.SetActive(true);

            UpdatePlayerPrefs();
            Time.timeScale = 0f;
        }
    }

    public void NextLevel(string level)
    {
        Time.timeScale = 1f;

        Application.LoadLevel(level);

    }
    // Congratulations you played yourself!!

    public void RestartLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
        Time.timeScale = 1f; 
    }

    public void QuitToMenu()
    {
        Application.LoadLevel("sceneMain");
        Time.timeScale = 1f;    
    }

    void UpdatePlayerPrefs()
    {
        string requestScore = ("levelScore_" + levelNum.ToString());
        int previousScore;
        previousScore = PlayerPrefs.GetInt(requestScore);

        PlayerPrefs.SetInt("levelsCompleted", levelNum + 1);

        if (gc.pickupNum < previousScore)
        {
            PlayerPrefs.SetInt("levelScore_1", gc.pickupNum);
        } 
        gc.pickupNum = 0;
    }
}
