﻿using UnityEngine;
using System.Collections;

public class DamagedPlatformScript : MonoBehaviour {

    public float waitTime;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            StartCoroutine(CrumbleTime());
        }
    }

    IEnumerator CrumbleTime()
    {
        yield return new WaitForSeconds(waitTime);

        gameObject.SetActive(false);
    }
}
