﻿using UnityEngine;
using System.Collections;

public class HazardsScript : MonoBehaviour {

    public GameObject player;
    public GameObject deathCanvas;

    private SoundManagerScript sm;

    void Start()
    {
        sm = GameObject.FindWithTag("GameController").GetComponent<SoundManagerScript>();

        player = GameObject.FindWithTag("Player");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            sm.PlayClips(0);
            Destroy(player);

            deathCanvas.SetActive(true);
        }
    }
}
