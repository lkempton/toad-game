﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour
{
    private bool onGround;
    private int waitTime = 1;
    private bool isWaiting = false;

    private Rigidbody2D rbody;

    [SerializeField]
    private GameObject dot;

    [SerializeField]
    private int dotNum = 10;

    [SerializeField]
    private float force = 5.0f;

    [SerializeField]
    private float modify = 1.0f;

    private GameObject[] line;

    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();

        onGround = true;

        CreateLine();
    } 

    void Update()
    {
        Vector3 tempPos = Camera.main.WorldToScreenPoint(transform.position);

        tempPos.z = 0;

        if (onGround == true && isWaiting == false)
        {

            Vector2 tempDirection = (Input.mousePosition - tempPos).normalized;

            if (Input.GetMouseButton(0))
            {
                Aim(tempDirection);
            }

            if (Input.GetMouseButtonUp(0))
            {
                transform.GetComponent<Rigidbody2D>().AddForce(tempDirection * force, ForceMode2D.Impulse);

                onGround = false;

                StartCoroutine(WaitingTime());

                for (int i = 0; i < line.Length; i++)
                {
                    line[i].SetActive(false);
                }
            }
        }
    }

    void CreateLine()
    {
        line = new GameObject[dotNum];

        for (int i = 0; i < line.Length; i++)
        {
            GameObject tempObj = Instantiate(dot, Vector3.zero, Quaternion.identity) as GameObject;

            tempObj.SetActive(false);

            line[i] = tempObj;
        }
    }

    private void Aim(Vector2 _direction)
    {
        float voX = _direction.x * force;
        float voY = _direction.y * force;

        for (int i = 0; i < line.Length; i++)
        {
            float t = i * modify;

            line[i].transform.position = new Vector3(transform.position.x + voX * t, transform.position.y + ((voY * t) - (0.5f * 9.81f * t * t)), 0.0f);

            line[i].SetActive(true);
        }
    }

    IEnumerator WaitingTime()
    {
        isWaiting = true;
        yield return new WaitForSeconds(waitTime);
        isWaiting = false;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            onGround = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            rbody.velocity = new Vector2(0.0f, rbody.velocity.y);
        }
    }
}
