﻿using UnityEngine;
using System.Collections;

public class ButtonPressScript : MonoBehaviour {

    public GameObject LinkedWall;

    private Animator anim;

    private MovingPlatformScript mp;

    void Start()
    {
        anim = GetComponent<Animator>();

        if (GameObject.FindWithTag("Moveable") != null)
        {
            mp = GameObject.FindWithTag("Moveable").GetComponent<MovingPlatformScript>();
        }
        
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            anim.SetTrigger("ButtonPress");

            if (LinkedWall.tag == "Ground" || LinkedWall.tag == "Removeable")
            {
                LinkedWall.SetActive(false);
            }
            else if (LinkedWall.tag == "Moveable")
            {
                mp.isActive = true;
            }
            
        }
    }
}
