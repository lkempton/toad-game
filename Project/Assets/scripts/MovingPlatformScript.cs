﻿using UnityEngine;
using System.Collections;

public class MovingPlatformScript : MonoBehaviour {

    public Transform movingPlatform;
    public Transform pos1;
    public Transform pos2;

    public Vector3 newPos;
    public float smoothing;
    public float resetTime;

    public bool isActive = true;

    public string currentState;

    void Start()
    {
        ChangeTarget();
        newPos = pos1.position;
    }

    void FixedUpdate()
    {
        if (isActive == true)
        {
            movingPlatform.position = Vector3.Lerp(movingPlatform.position, newPos, smoothing * Time.deltaTime);
        }
    }

    void ChangeTarget()
    {
        if (currentState == "Moving to Pos1")
        {
            currentState = "Moving to Pos2";
            newPos = pos2.position;
        }
        else if (currentState == "Moving to Pos2")
        {
            currentState = "Moving to Pos1";
            newPos = pos1.position;
        }
        else if (currentState == "")
        {
            currentState = "Moving to Pos1";
        }

        Invoke("ChangeTarget", resetTime);
    }


}
