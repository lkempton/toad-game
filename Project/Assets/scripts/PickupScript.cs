﻿using UnityEngine;
using System.Collections;

public class PickupScript : MonoBehaviour {

    private GameControllerScript gc;

    void Start()
    {
        gc = GameObject.FindWithTag("GameController").GetComponent<GameControllerScript>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            gc.pickupNum++;
            Destroy(gameObject);
        }
    }
}
