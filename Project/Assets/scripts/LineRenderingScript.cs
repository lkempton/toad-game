﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineRenderingScript : MonoBehaviour {

    public GameObject player;

    public Vector2 startPos;
    public Vector2 endPos;
    private LineRenderer lR;

    private Vector3 target;
    public float range = 30f;
  
    void Start()
    {
        range = 30f;
        lR = GetComponent<LineRenderer>();
        startPos = player.transform.position;
        endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

    }

    void Update()
    {
        startPos = player.transform.position;
        endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        lR.SetPosition(0, startPos);
        lR.SetPosition(1, endPos);

        Vector2 pos = target - transform.position;

        if (pos.magnitude >= range) 
        {
           lR.SetColors(Color.red, Color.red);
        }
        else
        {
           lR.SetColors(Color.green, Color.green);
        }
    }
}
